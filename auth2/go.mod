module armani_auth

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/jackc/pgx/v4 v4.10.1
	go.mongodb.org/mongo-driver v1.5.0 // indirect
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
)
